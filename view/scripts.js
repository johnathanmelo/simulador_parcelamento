$( document ).ready(function() {
 
	// procedimento executado quando o botão de submissão do formulário ("simular") é clicado.
    $( "#form_simulador" ).submit(function( e ) {

		e.preventDefault();
		
		// desabilita botão de simular e altera seu texto.
		$( "input[name=simulate]" ).prop("disabled",true);
		$( "input[name=simulate]" ).val("Simulando...");
		
		// captura os dados de entrada inseridos pelo usuário.
		var input_name = $( "input[name=name]" ).val().replace(/[áàâã]/g,'a').replace(/[ÁÀÂÃ]/g,'A').replace(/[éèê]/g,'e').replace(/[ÉÈÊ]/g,'E').replace(/[íì]/,'i').replace(/[ÍÌ]/,'I').replace(/[óòôõ]/g,'o').replace(/[ÓÒÔÕ]/g,'O').replace(/[úùû]/g,'u').replace(/[ÚÙÛ]/g,'U');		
        var arr = { name: input_name, price: $( "input[name=price]" ).val() , months: $( "input[name=months]:checked" ).val() };
		
		// envia os dados do formulário para o servidor.
		$.ajax({			
			headers: { 
				'Accept': 'application/json',
				'Content-Type': 'application/json' 
			},				
			'type': "POST",			
			'url': "http://127.0.0.1:8080/WebServiceRest/rest/service/simulate",			
			'data': JSON.stringify(arr),		
			
			// procedimento executado quando o servidor retorna o resultado da simulação.
			'success': function(data){			

					// torna os elementos invisíveis.
					$( ".name" ).css({"display":"none"});
					$( ".price" ).css({"display":"none"});
					$( ".months" ).css({"display":"none"});
					$( ".simulate" ).css({"display":"none"});
					
					// torna os elementos visíveis.
					$( ".name_text" ).css({"display":"block" });
					$( ".price_text" ).css({"display":"block" });			
					$( ".interest" ).css({"display":"block" });
					$( ".months_text" ).css({"display":"block" });				
					$( ".payment" ).css({"display":"block" });
					$( ".url" ).css({"display":"block" });
					
					// insere as informações provenientes do servidor para os elementos visíveis.
					$( ".name_text" ).text( "Cliente: " + data.name);
					$( ".price_text" ).text( "Valor do Imóvel: R$ " + data.price.toFixed(2).toString().replace(".", ","));
					$( ".interest" ).text( "Taxa de Juros: " + data.interest*100 + "%");
					$( ".months_text" ).text( "Parcelas: " + data.months + " meses");				
					$( ".payment" ).text( "Valor da Parcela Mensal: R$ " + data.payment.toFixed(2).toString().replace(".", ","));
					$( ".url" ).text( "URL da Simulação: " + "http://127.0.0.1:8080/WebServiceRest/rest/service/getSimulation/" + data.code );

			},
			
			// procedimento executado caso ocorra algum erro durante a comunicação com o servidor.
			'error': function(jqXHR, textStatus, errorThrown){
			
				// habilita novamente o botão de simular e volta com seu texto original.
				$( "input[name=simulate]" ).prop("disabled",false);
				$( "input[name=simulate]" ).val("Simular");
				
				alert("Erro ao estabelecer conexão. Tente novamente mais tarde.");
				
			},
			 'dataType': 'json'			 
		});			
    }); 
});