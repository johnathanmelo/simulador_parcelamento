package br.com.simulador.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
 
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.simulador.http.Simulation;
import br.com.simulador.repository.SimulationRepository;
import br.com.simulador.repository.InterestRepository;
import br.com.simulador.repository.entity.InterestEntity;
import br.com.simulador.repository.entity.SimulationEntity;


/**
 * Essa classe vai expor os métodos para serem acessados via HTTP.
 * 
 * @Path - Caminho para a chamada da classe que vai representar o serviço.
 * */
@Path("/service")
public class ServiceController {
 
	private final  SimulationRepository repository = new SimulationRepository();
	private final  InterestRepository interestRepository = new InterestRepository();
 
	/**
	 * @Consumes - determina o formato dos dados a serem postados.
	 * @Produces - determina o formato dos dados a serem retornados.
	 * 
	 * Esse método salva a simulação e retorna o resultado.
	 * */
	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/simulate")
	public Simulation Simulate(Simulation simulation){
 
		SimulationEntity entity = new SimulationEntity();		
			
		// Obtém os juros referente ao número de meses.
		List<InterestEntity> i = interestRepository.GetInterestByMonth(simulation.getMonths());
			
		// Calcula o valor da parcela mensal (payment).
		double initialPrice = simulation.getPrice();
		double interest = i.get(0).getInterest();
		double finalPrice = initialPrice + (initialPrice * interest);
		double payment = finalPrice / (double)simulation.getMonths();
			
		// Salva os dados da simulação inseridos pelo usuário.
		entity.setName(simulation.getName());
		entity.setPrice(simulation.getPrice());
		entity.setMonths(simulation.getMonths());
		entity.setPayment(payment);

		repository.Save(entity); 
		
		// Retorna resultado da simulação ao cliente.
		return new Simulation(entity.getCode(), entity.getName(), entity.getPrice(), entity.getMonths(), interest, entity.getPayment());
 
	}
 
	/**
	 * Esse método busca uma simulação pelo código e retorna o resultado em HTML.
	 * */
	@GET
	@Produces({MediaType.TEXT_HTML})
	@Path("/getSimulation/{code}")		
	public String GetSimulation(@PathParam("code") Integer code){
 
		SimulationEntity entity = repository.GetSimulation(code);	
		
		// Se código for válido, retorna o resultado da simulação em HTML.
		if(entity != null) {
			
			List<InterestEntity> i = interestRepository.GetInterestByMonth(entity.getMonths());
			
			String name = entity.getName();
			String price = round(entity.getPrice(), 2).toString().replace(".",",");
			String months = entity.getMonths().toString();
			Double p_interest = i.get(0).getInterest() * 100;
			String interest = p_interest.toString().replace(".",",");
			String payment = round(entity.getPayment(), 2).toString().replace(".",",");
		
			// Cria página HTML com os dados da simulação.
			String HTML = 
					"<html><title>Simulador de Parcelamento</title>" + 
					"<meta charset=\"utf-8\">"+
					"<body style=\"margin: 0; padding: 0;\">" + 
					"<div style=\"font-size: 16px; background: linear-gradient(white, lightgray, lightgray); max-width: 500px; margin: 10px auto; padding: 0 10px 10px 10px; border: 1px solid; border-radius: 10px;\">"+
						"<h1 style=\"color: #333; text-align: center;\">Simulador de Parcelamento</h1>" + 
						"<p>Cliente: " + name +"</p>"+
						"<p>Valor do Imóvel: R$ " + price +"</p>"+
						"<p>Parcelas: " + months + " meses</p>"+
						"<p>Taxa de Juros: " + interest + "%</p>"+
						"<p>Valor da Parcela Mensal: R$ " + payment + "</p>"+
					"</div></body>" +
					"<footer style=\"text-align: center;max-width: 500px;margin: 0 auto;padding: 0 10px 10px 10px;\"><p>Copyright &copy Johnathan Mayke Melo Neto</p></footer>" +
					"</html>";
			
			return HTML;
			
		}
			
		// Se código for inválido, retorna mensagem de erro.
		String InvalidHTML =
				"<html><meta charset=\"utf-8\">" +
				"<h1>URL Inválida</h1></html>";
		
		return InvalidHTML;
	}
 
	/**
	 * Deleta uma simulação pelo código.
	 * */
	@DELETE
	@Produces("application/json; charset=UTF-8")
	@Path("/delete/{code}")	
	public String Delete(@PathParam("code") Integer code){
 
		try {
 
			repository.Delete(code);
 
			return "Registro excluído com sucesso!";
 
		} catch (Exception e) {
 
			return "Erro ao excluir o registro! " + e.getMessage();
		}
 
	}
	
	/**
	 * Esse método auxiliar trunca um valor double para o número de casas desejadas.
	 * */
	public Double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

}