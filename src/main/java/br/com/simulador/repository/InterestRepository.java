package br.com.simulador.repository;


import java.util.List;
 
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.simulador.repository.entity.InterestEntity;



public class InterestRepository {

	private final EntityManagerFactory entityManagerFactory;

	private final EntityManager entityManager;

	public InterestRepository(){

		// Cria o EntityManagerFactory com as propriedades do arquivo persistence.xml
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_unit_db_simulador");

		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	/**
	 * CRIA UM NOVO REGISTRO NO BANCO DE DADOS.
	 * */
	public void Save(InterestEntity interestEntity){
 
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(interestEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * ALTERA UM REGISTRO CADASTRADO.
	 * */
	public void Update(InterestEntity interestEntity){

		this.entityManager.getTransaction().begin();
		this.entityManager.merge(interestEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * RETORNA TODOS OS REGISTROS CADASTRADOS NO BANCO DE DADOS .
	 * */
	@SuppressWarnings("unchecked")
	public List<InterestEntity> AllInterests(){

		return this.entityManager.createQuery("SELECT p FROM InterestEntity p ORDER BY p.months").getResultList();
	}

	/**
	 * CONSULTA O VALOR DE JUROS REFERENTE AO MÊS INSERIDO.
	 * */
	@SuppressWarnings("unchecked")
	public List<InterestEntity> GetInterestByMonth(Integer months){
 
		return this.entityManager.createQuery("SELECT p FROM InterestEntity p WHERE p.months = :months").setParameter("months", months).setMaxResults(1).getResultList();
	}

	/**
	 * CONSULTA UM REGISTRO CADASTRADO PELO CÓDIGO.
	 * */
	public InterestEntity GetInterest(Integer code){
 
		return this.entityManager.find(InterestEntity.class, code);
	}

	/**
	 * DELETA UM REGISTRO PELO CÓDIGO.
	**/
	public void Delete(Integer code){

		InterestEntity interest = this.GetInterest(code);

		this.entityManager.getTransaction().begin();
		this.entityManager.remove(interest);
		this.entityManager.getTransaction().commit();

	}
	
}