package br.com.simulador.repository;
  
import java.util.List;
 
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.simulador.repository.entity.SimulationEntity;
 
public class SimulationRepository {
 
	private final EntityManagerFactory entityManagerFactory;
 
	private final EntityManager entityManager;
 
	public SimulationRepository(){
 
		// Cria EntityManagerFactory com as propriedades do arquivo persistence.xml
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_unit_db_simulador");
 
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}
 
	/**
	 * CRIA UM NOVO REGISTRO NO BANCO DE DADOS.
	 * */
	public void Save(SimulationEntity simulationEntity){
 
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(simulationEntity);
		this.entityManager.getTransaction().commit();
	}
 
	/**
	 * ALTERA UM REGISTRO CADASTRADO.
	 * */
	public void Update(SimulationEntity simulationEntity){
 
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(simulationEntity);
		this.entityManager.getTransaction().commit();
	}
 
	/**
	 * RETORNA TODAS AS SIMULAÇÕES CADASTRADAS NO BANCO DE DADOS.
	 * */
	@SuppressWarnings("unchecked")
	public List<SimulationEntity> AllSimulations(){
 
		return this.entityManager.createQuery("SELECT p FROM SimulationEntity p ORDER BY p.name").getResultList();
	}
 
	/**
	 * CONSULTA UMA SIMULAÇÃO CADASTRADA PELO CÓDIGO.
	 * */
	public SimulationEntity GetSimulation(Integer code){
 
		return this.entityManager.find(SimulationEntity.class, code);
	}
 
	/**
	 * DELETA UM REGISTRO PELO CÓDIGO.
	**/
	public void Delete(Integer code){
 
		SimulationEntity simulation = this.GetSimulation(code);
 
		this.entityManager.getTransaction().begin();
		this.entityManager.remove(simulation);
		this.entityManager.getTransaction().commit();
 
	}
}