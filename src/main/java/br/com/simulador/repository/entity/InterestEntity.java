package br.com.simulador.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_interest")
public class InterestEntity {
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="code")
	private Integer code;
 
	@Column(name="months")
	private Integer months;
	
	@Column(name="interest")
	private Double interest;
 
 
	public Integer getCode() {
		return code;
	}
 
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public Integer getMonths() {
		return months;
	}
 
	public void setMonths(Integer months) {
		this.months = months;
	}
	
	public Double getInterest() {
		return interest;
	}
 
	public void setInterest(Double interest) {
		this.interest = interest;
	}
	
}