package br.com.simulador.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name="tb_simulation")
public class SimulationEntity {
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="code")
	private Integer code;
 
	@Column(name="name")	
	private String  name;
 
	@Column(name="price")
	private Double  price;
	
	@Column(name="months")
	private Integer  months;
	
	@Column(name="payment")
	private Double  payment;
 
 
	public Integer getCode() {
		return code;
	}
 
	public void setCode(Integer code) {
		this.code = code;
	}
 
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
 
	public Double getPrice() {
		return price;
	}
 
	public void setPrice(Double price) {
		this.price = price;
	}
	
	public Integer getMonths() {
		return months;
	}
 
	public void setMonths(Integer months) {
		this.months = months;
	}
	
	public Double getPayment() {
		return payment;
	}
 
	public void setPayment(Double payment) {
		this.payment = payment;
	}
 
}