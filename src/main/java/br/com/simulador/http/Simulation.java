package br.com.simulador.http;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Simulation {
	
	private int code;
	private String name;
	private Double price;
	private Integer months;
	private Double interest;
	private Double payment;

	public Simulation(){

	}

	public Simulation(int code, String name, Double price, Integer months, Double interest, Double payment) {
		super();
		this.code = code;
		this.name = name;
		this.price = price;
		this.months = months;
		this.interest = interest;
		this.payment = payment;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public double getPayment() {
		return payment;
	}

	public void setPayment(double payment) {
		this.payment = payment;
	}
	
}